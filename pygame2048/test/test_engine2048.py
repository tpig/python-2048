import sys
sys.path.append("..")
from engine2048 import Engine2048


def test_is_board_full(engine):
    data = engine.data

    engine.data = [[1] * 4 for _ in range(4)]
    assert engine.is_board_full(), "check_is_board_full method fail"

    engine.data[2][1] = 0
    assert not engine.is_board_full(),"check_is_board_full method fail"

    engine.data = data


def print_matrix(engine):
    for row in engine.data:
        print(row)

def main():
    engine = Engine2048()

    test_is_board_full(engine)

    print("*" * 20, "add new tile", "*" * 20)
    engine.add_new_tile()
    print_matrix(engine)

    print("*" * 20, "reserve matrix", "*" * 20)
    engine.data = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15, 16]
    ]
    print_matrix(engine)
    print("-" * 80)
    engine.reserve_matrix_left_right()
    print_matrix(engine)

    print("*" * 20, "transpose matrix", "*" * 20)
    engine.data = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15, 16]
    ]
    print_matrix(engine)
    print("-" * 80)
    engine.transpose_matrix()
    print_matrix(engine)
    print("-" * 80)
    engine.transpose_matrix()
    print_matrix(engine)



if __name__ == '__main__':
    main()