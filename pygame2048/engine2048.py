"""

- add_new_tile()
- find_empty_tile()

- stack_left()
- combine_left()
- reserve_matrix_left_right()
- transpose_matrix()

- is_game_over()
- is_board_full()
- has_left_right_movable()
- has_up_down_movable()
"""
import random


class Engine2048:
    def __init__(self):
        self.data = [[0] * 4 for _ in range(4)]

    def is_board_full(self):
        if any(0 in row for row in self.data):
            return False
        return True

    def find_empty_tile(self):
        row = random.randint(0, 3)
        col = random.randint(0, 3)

        if self.is_board_full():
            return None

        while self.data[row][col] != 0:
            row = random.randint(0, 3)
            col = random.randint(0, 3)

        return row, col

    def add_new_tile(self):
        if self.is_board_full():
            return None
        row, col = self.find_empty_tile()
        self.data[row][col] = random.choice([2, 4])

    def reserve_matrix_left_right(self):
        new_matrix = [[0] * 4 for _ in range(4)]
        for row in range(4):
            for col in range(4):
                new_matrix[row][col] = self.data[row][3 - col]
        self.data = new_matrix

    def transpose_matrix(self):
        new_matrix = [[0] * 4 for _ in range(4)]
        for row in range(4):
            for col in range(4):
                new_matrix[row][col] = self.data[col][row]
        self.data = new_matrix

def main():

    eng = Engine2048()
    print(eng.find_empty_tile())

if __name__ == '__main__':
    main()